import car1 from "./components/CarItem/imgs/car1.png";
import car2 from "./components/CarItem/imgs/car2.png";
import car3 from "./components/CarItem/imgs/car3.png";
import car4 from "./components/CarItem/imgs/car4.png";
import car5 from "./components/CarItem/imgs/car5.png";
import car6 from "./components/CarItem/imgs/car6.png";
import car7 from "./components/CarItem/imgs/car7.png";
import car8 from "./components/CarItem/imgs/car1.png";

export const cars_data = [
  {
    id: 1,
    name: " Mazzeraati",
    modal: 2016,
    price: 123214124,
    img: car1,
    supplier: {
      name: "avis",
      ratting: "8+",
      location: true
    },
    specs: {
      withAC: true,
      at: true,
      mgbox: true,
      fourD: true,
      fuelType: "Deisel",
      seats: 4,
      dmgexcess: 5000
    }
  },
  {
    id: 2,
    name: " Mazzeraati",
    modal: 2016,
    price: 123214124,
    img: car2,
    supplier: {
      name: "sixt",
      ratting: "5+",
      location: false
    },
    specs: {
      withAC: true,
      at: true,
      mgbox: false,
      fourD: true,
      fuelType: "Deisel",
      seats: 4,
      dmgexcess: 3000
    }
  },
  {
    id: 3,
    name: " Buggati",
    modal: 2017,
    price: 123214124,
    img: car3,
    supplier: {
      name: "Budget",
      ratting: "8+",
      location: true
    },
    specs: {
      withAC: true,
      at: true,
      mgbox: true,
      fourD: true,
      fuelType: "Deisel",
      seats: 4,
      dmgexcess: 2000
    }
  },
  {
    id: 4,
    name: " Lamborghini",
    modal: 2016,
    img: car4,
    price: 123214124,
    supplier: {
      name: "avis",
      ratting: "7+",
      location: true
    },
    specs: {
      withAC: true,
      at: false,
      mgbox: true,
      fourD: false,
      fuelType: "Deisel",
      seats: 4,
      dmgexcess: 5000
    }
  },
  {
    id: 5,
    name: " Mazzeraati",
    modal: 2016,
    img: car5,
    price: 123214124,
    supplier: {
      name: "Budget",
      ratting: "5+",
      location: false
    },
    specs: {
      withAC: false,
      at: false,
      mgbox: true,
      fourD: false,
      fuelType: "Deisel",
      seats: 4,
      dmgexcess: 5000
    }
  },
  {
    id: 6,
    name: " Mazzeraati",
    modal: 2016,
    img: car6,
    price: 123214124,
    supplier: {
      name: "avis",
      ratting: "8+",
      location: true
    },
    specs: {
      withAC: true,
      at: true,
      mgbox: true,
      fourD: false,
      fuelType: "Deisel",
      seats: 4,
      dmgexcess: 5000
    }
  },
  {
    id: 7,
    name: " Mazzeraati",
    modal: 2016,
    img: car7,
    price: 123214124,
    supplier: {
      name: "avis",
      ratting: "8+",
      location: true
    },
    specs: {
      withAC: true,
      at: true,
      mgbox: true,
      fourD: false,
      fuelType: "Deisel",
      seats: 4,
      dmgexcess: 5000
    }
  },
  {
    id: 8,
    name: " Mazzeraati",
    modal: 2016,
    img: car8,
    price: 123214124,
    supplier: {
      name: "avis",
      ratting: "8+",
      location: true
    },
    specs: {
      withAC: true,
      at: true,
      mgbox: true,
      fourD: false,
      fuelType: "Deisel",
      seats: 4,
      dmgexcess: 5000
    }
  }
];
