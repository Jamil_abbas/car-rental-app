export const initialState = fromJS({
  list: {
    isLoggedIn: false
  }
});

const navigationReducer = (state = initialState, action) => {
  switch (action.type) {
    case constants.FETCH_LIST:
      return state.setIn(["list", "loading"], true);

    default:
      return state;
  }
};

export default appReducer;
