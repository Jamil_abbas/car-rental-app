import React from "react";
import { Layout, Menu, Avatar, Dropdown } from "antd";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { isLoggedInUser } from "./action";
// import toJS from "../../components/common/ToJs";

const { Header } = Layout;

class Navigation extends React.Component {
  render() {
    const menu = (
      <Menu>
        <Menu.Item key="0">Action One</Menu.Item>
        <Menu.Item key="1">Action two</Menu.Item>
        <Menu.Divider />
        <Menu.Item key="3">Action three</Menu.Item>
      </Menu>
    );
    return (
      <Header className="header">
        <div className="logo" />
        <div style={{ float: "right" }}>
          <Menu theme="dark" mode="horizontal" style={{ lineHeight: "64px" }}>
            <Menu.Item key="1">
              <Link to="/">Home</Link>{" "}
            </Menu.Item>
            <Menu.Item key="2">
              <Link to="/contact">Contact</Link>{" "}
            </Menu.Item>
              <Menu.Item key="3">
                <Link to="/signUp">Sign Up</Link>
              </Menu.Item>
              <Menu.Item key="4">
                <Link to="/login">Login</Link>
              </Menu.Item>
            <Dropdown
              overlay={menu}
              trigger={["click"]}
              style={{ float: "left" }}
            >
              <Avatar style={{ backgroundColor: "#87d068" }} icon="user" />
            </Dropdown>
          </Menu>
        </div>
      </Header>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onUserLogin: () => dispatch(isLoggedInUser())
  };
};

export default connect(
  null,
  mapDispatchToProps
)(Navigation);
