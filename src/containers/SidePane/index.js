import React from "react";
import { connect } from 'react-redux';
import * as actions from '../App/actions'
import { Layout, Menu, Checkbox, Icon } from "antd";

const { Sider } = Layout;

class SidePane extends React.Component {


  onChange = (e) => {
    this.props.setFilters(e.target.value, e.target.checked)
  }
  render () {
    const { SubMenu } = Menu;
    return (
      <Sider width={200} style={{ background: "#fff" }}>
        <Menu
          mode="inline"
          defaultSelectedKeys={["1"]}
          defaultOpenKeys={["sub1"]}
          style={{ height: "100%" }}
        >
          <SubMenu
            key="sub1"
            title={
              <span>
                <Icon type="search" />
                Brand
            </span>
            }
          >
            <Menu.Item key="1">
              <Checkbox onChange={this.onChange} value="Volvo">Volvo</Checkbox>
            </Menu.Item>
            <Menu.Item key="2">
              <Checkbox onChange={this.onChange} value="Toyota">Toyota</Checkbox>
            </Menu.Item>
            <Menu.Item key="3">
              <Checkbox onChange={this.onChange} value="Kia">Kia</Checkbox>
            </Menu.Item>
            <Menu.Item key="4">
              <Checkbox onChange={this.onChange} value="Bugatti">Bugatti</Checkbox>
            </Menu.Item>
            <Menu.Item key="5">
              <Checkbox onChange={this.onChange} value="Tesla">Tesla</Checkbox>
            </Menu.Item>
            <Menu.Item key="6">
              <Checkbox onChange={this.onChange} value="Volkswagen">Volkswagen</Checkbox>
            </Menu.Item>
            <Menu.Item key="7">
              <Checkbox onChange={this.onChange} value="Citroen">Citroen</Checkbox>
            </Menu.Item>
            <Menu.Item key="8">
              <Checkbox onChange={this.onChange} value="BMW">BMW</Checkbox>
            </Menu.Item>
          </SubMenu>
          <SubMenu
            key="sub2"
            title={
              <span>
                <Icon type="bar-chart" />
                Supplier Rating
            </span>
            }
          >
            <Menu.Item key="9">
              <Checkbox onChange={this.onChange}>Excellent: 8+</Checkbox>
            </Menu.Item>
            <Menu.Item key="10">
              <Checkbox onChange={this.onChange}>Very good: 7+</Checkbox>
            </Menu.Item>
          </SubMenu>
          <SubMenu
            key="sub3"
            title={
              <span>
                <Icon type="home" />
                Supplier Location
            </span>
            }
          >
            <Menu.Item key="11">
              <Checkbox onChange={this.onChange}>Shuttle Bus</Checkbox>
            </Menu.Item>
          </SubMenu>
          <SubMenu
            key="sub5"
            title={
              <span>
                <Icon type="file" />
                Fuel Policy
            </span>
            }
          >
            <Menu.Item key="17">
              <Checkbox onChange={this.onChange} value="gas">Gas</Checkbox>
            </Menu.Item>
            <Menu.Item key="18">
              <Checkbox onChange={this.onChange} value="diesel">Diesel</Checkbox>
            </Menu.Item>
            <Menu.Item key="19">
              <Checkbox onChange={this.onChange} value="electricity">Electricity</Checkbox>
            </Menu.Item>
          </SubMenu>
          <SubMenu
            key="sub4"
            title={
              <span>
                <Icon type="home" />
                Seats
            </span>
            }
          >
            <Menu.Item key="13">
              <Checkbox onChange={this.onChange} value={4}>4 Seat</Checkbox>
            </Menu.Item>
            <Menu.Item key="14">
              <Checkbox onChange={this.onChange} value={5}>5 Seat</Checkbox>
            </Menu.Item>
            <Menu.Item key="123">
              <Checkbox onChange={this.onChange} value={6}>6+ Seat</Checkbox>
            </Menu.Item>
          </SubMenu>
          <SubMenu
            key="sub6"
            title={
              <span>
                <Icon type="car" />
                Car specifications
            </span>
            }
          >
            <Menu.Item key="21">
              <Checkbox onChange={this.onChange}>With Air-conditioning</Checkbox>
            </Menu.Item>
            <Menu.Item key="22">
              <Checkbox onChange={this.onChange}>Automatic transmission</Checkbox>
            </Menu.Item>
            <Menu.Item key="23">
              <Checkbox onChange={this.onChange}>Manual Gearbox</Checkbox>
            </Menu.Item>
            <Menu.Item key="24">
              <Checkbox onChange={this.onChange}>4+ Doors</Checkbox>
            </Menu.Item>
            <Menu.Item key="25">
              <Checkbox onChange={this.onChange}>Diesel</Checkbox>
            </Menu.Item>
          </SubMenu>
          <SubMenu
            key="sub7"
            title={
              <span>
                <Icon type="safety" />
                Damage Excess
            </span>
            }
          >
            <Menu.Item key="26">
              <Checkbox onChange={this.onChange}>DKK5000.00 OR LESS</Checkbox>
            </Menu.Item>
            <Menu.Item key="27">
              <Checkbox onChange={this.onChange}>DKK6250.00 OR LESS</Checkbox>
            </Menu.Item>
          </SubMenu>
        </Menu>
      </Sider>
    );
  }
};

const mapDispatchToProps = dispatch => {
  return {
    setFilters: (value, checked) => dispatch(actions.setFilters(value, checked))
  }
}

export default connect(null, mapDispatchToProps)(SidePane);
