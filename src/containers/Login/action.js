//Login Actions
import { LOGIN_REQUEST, LOGIN_SUCCESS } from "./constants";

export function loginRequest(data) {
  console.log("LOGIN DATA", data);
  return {
    type: LOGIN_REQUEST,
    payload: data
  };
}

export function loginSuccess(data) {
  console.log("LOGIN success", data);
  return {
    type: LOGIN_SUCCESS,
    payload: data
  };
}
