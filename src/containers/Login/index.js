import React from "react";
import { Form, Icon, Input, Button, Checkbox } from "antd";
import { Row, Col } from "antd";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { loginRequest } from "./action";
import "./style.css";
class NormalLoginForm extends React.Component {
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      const { onLoginRequest, history } = this.props;
      if (!err) {
        if (values.username === "admin@admin.com") {
          onLoginRequest(values, history);
          this.props.history.push("/admin");
        } else {
          onLoginRequest(values, history);
          this.props.history.push("/profile");
        }
      }
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Row className="logincontainer">
        <Col span={8} offset={8}>
          <div className="loginform">
            <h1 style={{ textAlign: "center" }}>Login</h1>
            <Form
              onSubmit={this.handleSubmit}
              className="login-form"
              style={{ marginTop: "4rem" }}
            >
              <Form.Item>
                {getFieldDecorator("username", {
                  rules: [
                    { required: true, message: "Please input your username!" }
                  ]
                })(
                  <Input
                    prefix={
                      <Icon type="user" style={{ color: "rgba(0,0,0,.25)" }} />
                    }
                    placeholder="Username"
                  />
                )}
              </Form.Item>
              <Form.Item>
                {getFieldDecorator("password", {
                  rules: [
                    { required: true, message: "Please input your Password!" }
                  ]
                })(
                  <Input
                    prefix={
                      <Icon type="lock" style={{ color: "rgba(0,0,0,.25)" }} />
                    }
                    type="password"
                    placeholder="Password"
                  />
                )}
              </Form.Item>
              <Form.Item>
                {getFieldDecorator("remember", {
                  valuePropName: "checked",
                  initialValue: true
                })(<Checkbox>Remember me</Checkbox>)}
                {/* <a className="login-form-forgot" href="#">
                  Forgot password
                </a> */}
              </Form.Item>
              <Form.Item>
                <Button
                  type="primary"
                  htmlType="submit"
                  // loading={false}
                  className="login-form-button"
                >
                  Log in
                </Button>
              </Form.Item>
              <Form.Item>
                Or <Link to="/signUp">register now!</Link>
              </Form.Item>
            </Form>
          </div>
        </Col>
      </Row>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onLoginRequest: data => dispatch(loginRequest(data))
  };
};

const WrappedNormalLoginForm = Form.create({ name: "normal_login" })(
  NormalLoginForm
);
export default connect(
  null,
  mapDispatchToProps
)(WrappedNormalLoginForm);
