import React from "react";
import { connect } from "react-redux";

import { Layout, Spin, Breadcrumb, Card, Col, Row, Icon } from "antd";
import DemoGraph from "./DemoGraph";

const { Content, Footer } = Layout;

class Dashboard extends React.Component {
  render () {
    return (
      <Spin tip="Loading..." spinning={false}>
        <Layout>
          <Content style={{ padding: "0 50px" }}>
            <Breadcrumb style={{ margin: "40px 0" }} />
            <Layout style={{ padding: "24px 0", background: "#fff" }}>
              <Content style={{ padding: "0 24px", minHeight: 280 }}>
                <div style={{ background: 'rgb(240, 242, 245)', padding: '30px' }}>
                  <Row gutter={16}>
                    <Col span={6}>
                      <Card
                        title={
                          <div>
                            <span>Number of Users</span>
                            <Icon type='user' style={{
                              float: 'right',
                              marginTop: 5,
                              color: '#1890ff'
                            }} />
                          </div>
                        }
                        bordered={false}>
                        <div>
                          <span>20</span>
                        </div>
                      </Card>
                    </Col>
                    <Col span={6}>
                      <Card
                        title={
                          <div>
                            <span>User analysis</span>
                            <Icon type='bar-chart' style={{
                              float: 'right',
                              marginTop: 5,
                              color: '#1890ff'
                            }} />
                          </div>
                        }
                        bordered={false}>
                        <div>
                          <span>20</span>
                        </div>
                      </Card>
                    </Col>
                    <Col span={6}>
                      <Card
                        title={
                          <div>
                            <span>Total Products</span>
                            <Icon type='unordered-list' style={{
                              float: 'right',
                              marginTop: 5,
                              color: '#1890ff'
                            }} />
                          </div>
                        }
                        bordered={false}>
                        <div>
                          <span>100 Items In store for now</span>
                        </div>
                      </Card>
                    </Col>

                    <Col span={6}>
                      <Card
                        title={
                          <div>
                            <span>Active Orders</span>
                            <Icon type='crown' style={{
                              float: 'right',
                              marginTop: 5,
                              color: '#1890ff'
                            }} />
                          </div>
                        }
                        bordered={false}>
                        <div>
                          <span>10 Active orders</span>
                        </div>
                      </Card>
                    </Col>
                  </Row>
                </div>
                <div style={styles}>
                  <DemoGraph />
                </div>
              </Content>
            </Layout>
          </Content>
          <Footer style={{ textAlign: "center" }}>
            Created by J@mil @bb@s
          </Footer>
        </Layout>
      </Spin >
    );
  }
}

const styles = {
  margin: '20px 0px',
  background: 'rgb(240, 242, 245)',
  fontFamily: 'sans-serif',
  textAlign: 'center',
};

export default connect(
  null,
  null
)(Dashboard);
