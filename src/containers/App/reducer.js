import faker from "faker";
import * as constants from "./constants";
import { message } from "antd";
import { fromJS, Map, List } from "immutable";
import car1 from "../../components/CarItem/imgs/car1.png";
import car2 from "../../components/CarItem/imgs/car2.png";
import car3 from "../../components/CarItem/imgs/car3.png";
import car4 from "../../components/CarItem/imgs/car4.png";

export const initialState = fromJS({
  list: {
    loading: false,
    data: [],
    filters: [],
    error: null,
    cart: []
  }
});

const appReducer = (state = initialState, action) => {
  switch (action.type) {
    case constants.FETCH_LIST:
      return state.setIn(["list", "loading"], true);

    case constants.FETCH_LIST_SUCCESS: {
      const payload = List(
        action.payload.map(item =>
          item.brand === "Volvo"
            ? Map({
                ...item,
                deleteLoading: false,
                image: car1,
                supplier: {
                  name: "Testing Name",
                  ratting: faker.ratting,
                  location: true
                },
                specs: {
                  withAC: false,
                  at: true,
                  mgbox: true,
                  fourD: true,
                  fuelType: item.fueltype,
                  seats: item.seats,
                  dmgexcess: 5000
                }
              })
            : item.brand === "Citroen"
            ? Map({
                ...item,
                deleteLoading: false,
                image: car2,
                supplier: {
                  name: "Testing Name",
                  ratting: faker.ratting,
                  location: true
                },
                specs: {
                  withAC: false,
                  at: true,
                  mgbox: true,
                  fourD: true,
                  fuelType: item.fueltype,
                  seats: item.seats,
                  dmgexcess: 5000
                }
              })
            : item.brand === "Toyota"
            ? Map({
                ...item,
                deleteLoading: false,
                image: car3,
                supplier: {
                  name: "Testing Name",
                  ratting: faker.ratting,
                  location: true
                },
                specs: {
                  withAC: false,
                  at: true,
                  mgbox: true,
                  fourD: true,
                  fuelType: item.fueltype,
                  seats: item.seats,
                  dmgexcess: 5000
                }
              })
            : Map({
                ...item,
                deleteLoading: false,
                image: car4,
                supplier: {
                  name: "Testing Name",
                  ratting: faker.ratting,
                  location: true
                },
                specs: {
                  withAC: true,
                  at: true,
                  mgbox: true,
                  fourD: true,
                  fuelType: item.fueltype,
                  seats: item.seats,
                  dmgexcess: 5000
                }
              })
        )
      );

      return state
        .setIn(["list", "data"], payload)
        .setIn(["list", "loading"], false);
    }

    case constants.FETCH_LIST_ERROR:
      return state
        .setIn(["list", "loading"], false)
        .setIn(["list", "error"], action.error);

    case constants.SET_FILTER: {
      if (action.checked) {
        return state.updateIn(["list", "filters"], data =>
          data.push(action.value)
        );
      } else {
        const index = state.getIn(["list", "filters"]).indexOf(action.value);
        if (index > -1) {
          const data = state.getIn(["list", "filters"]).splice(index, 1);
          return state.setIn(["list", "filters"], data);
        }
        return state;
      }
    }

    case constants.ADD_TO_CART: {
      const index = state.getIn(["list", "cart"]).indexOf(action.id);
      if (index < 0) {
        message.success("Successfully added to the cart!!!");
        return state.updateIn(["list", "cart"], data => data.push(action.id));
      } else {
        message.info("Product already added into cart!!!!");
        return state;
      }
    }

    case constants.REMOVE_FROM_CART: {
      const index = state.getIn(["list", "cart"]).indexOf(action.id);
      return state.deleteIn(["list", "cart", index]);
    }

    default:
      return state;
  }
};
export default appReducer;
