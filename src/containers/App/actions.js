// APP Actions
import * as constants from "./constants";

export function getData() {
  return {
    type: constants.FETCH_LIST
  };
}

export function fetchListSuccess(data) {
  return {
    type: constants.FETCH_LIST_SUCCESS,
    payload: data
  };
}

export function fetchListError(error) {
  return {
    type: constants.FETCH_LIST_SUCCESS,
    error
  };
}

export function setFilters(value, checked) {
  return {
    type: constants.SET_FILTER,
    value,
    checked
  };
}

export function addToCart(id) {
  return {
    type: constants.ADD_TO_CART,
    id
  };
}

export function removeFromCart(id) {
  return {
    type: constants.REMOVE_FROM_CART,
    id
  };
}
