import { createSelector } from "reselect";
import { initialState } from "./reducer";

const selectCarDomain = state => state.get("carsData", initialState);

const makeSelectLoading = () =>
  createSelector(
    selectCarDomain,
    substate => substate.getIn(["list", "loading"])
  );

const makeSelectFilters = () =>
  createSelector(
    selectCarDomain,
    substate => substate.getIn(["list", "filters"])
  );

const makeSelectCarList = () =>
  createSelector(
    selectCarDomain,
    state => {
      const list = state.getIn(["list", "data"]).toJS();
      const brands = state.getIn(["list", "filters"]).toJS();
      if (brands.length)
        return list.filter(
          item =>
            brands.includes(item.brand) ||
            brands.includes(item.fueltype) ||
            brands.includes(item.seats)
        );
      else return list;
    }
  );
const makeSelectCountCartItems = () =>
  createSelector(
    selectCarDomain,
    state => {
      const cartItemsCount = state.getIn(["list", "cart"]).toJS().length;
      return cartItemsCount;
    }
  );
const makeSelectCartItemsList = () =>
  createSelector(
    selectCarDomain,
    state => {
      const cartItemList = state.getIn(["list", "cart"]).toJS();
      const items = state.getIn(["list", "data"]).toJS();
      return items.filter(item => cartItemList.includes(item.id));
    }
  );

export {
  makeSelectLoading,
  makeSelectFilters,
  makeSelectCarList,
  makeSelectCountCartItems,
  makeSelectCartItemsList
};
