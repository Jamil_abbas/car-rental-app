import React from "react";
import SidePane from "../SidePane";
import * as selectors from "./selectors";
import CarItem from "../../components/CarItem";
import { Layout, Spin, Breadcrumb, Icon, Badge } from "antd";
import toJS from "../../components/common/ToJs";
import { Link } from "react-router-dom";
import { getData, addToCart } from "./actions";
import { connect } from "react-redux";
import "./style.css";

class App extends React.Component {
  componentDidMount() {
    const { getCarsData } = this.props;
    getCarsData();
  }
  render() {
    const { Content, Footer } = Layout;
    const { carsDataNew, data, onAddToCart, cartItemsCount } = this.props;
    return (
      <Spin tip="Loading..." spinning={this.props.loading}>
        <Layout>
          <Content style={{ padding: "0 50px" }}>
            <Breadcrumb style={{ margin: "16px 0" }}>
              <Breadcrumb.Item>Home</Breadcrumb.Item>
              <Link to="/cart">
                <Badge count={cartItemsCount} className="badge" />
                <Icon
                  type="shopping-cart"
                  style={{ float: "right", fontSize: "32px" }}
                />
              </Link>
            </Breadcrumb>

            <Layout style={{ padding: "24px 0", background: "#fff" }}>
              <SidePane cars={carsDataNew} />
              <Content style={{ padding: "0 24px", minHeight: 280 }}>
                <div className="content-container">
                  {data &&
                    data.map(
                      (car, key) =>
                        (
                          <CarItem
                            {...car}
                            key={key}
                            name={car.name}
                            price={car.price}
                            model={car.modal}
                            description={car.description}
                            carimg={car.image}
                            onAddToCart={onAddToCart}
                          />
                        ) || (
                          <div>
                            <Icon
                              type="exclamation-circle"
                              style={{ fontSize: "16px", color: "red" }}
                            />
                            <p>No Item found!!!</p>
                          </div>
                        )
                    )}
                </div>
              </Content>
            </Layout>
          </Content>
          <Footer style={{ textAlign: "center" }} />
        </Layout>
      </Spin>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    // dispatching plain actions
    getCarsData: () => dispatch(getData()),
    onAddToCart: id => dispatch(addToCart(id))
  };
};

const mapStateToProps = state => {
  return {
    loading: selectors.makeSelectLoading()(state),
    data: selectors.makeSelectCarList()(state),
    cartItemsCount: selectors.makeSelectCountCartItems()(state),
    carsData: state.carsData
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(toJS(App));
