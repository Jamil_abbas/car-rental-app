import React from "react";
import { connect } from "react-redux";
import { Layout, Spin, Breadcrumb, Icon, Button, Table, Modal } from "antd";
import * as selectors from "../App/selectors";
import { removeFromCart } from "../App/actions";

const { confirm } = Modal;
const { Content, Footer } = Layout;

class Cart extends React.Component {
  showConfirm() {
    confirm({
      title: "Payment Details",
      content:
        "We will integrate the payment gateway once the backend is ready!!",
      onOk() {
        return new Promise((resolve, reject) => {
          setTimeout(Math.random() > 0.5 ? resolve : reject, 1000);
        }).catch(() => console.log("Oops errors!"));
      },
      onCancel() {}
    });
  }
  render() {
    const { cartItemsList } = this.props;
    const dataSource = cartItemsList;

    const columns = [
      {
        title: "Image",
        dataIndex: "image",
        key: "image",
        render: image => (
          <img src={image} height="50px" width="auto" alt="some car pic" />
        )
      },
      {
        title: "Brand",
        dataIndex: "brand",
        key: "brand"
      },
      {
        title: "Car Type",
        dataIndex: "cartype",
        key: "cartype"
      },
      {
        title: "Price",
        dataIndex: "price",
        key: "price"
      },
      {
        title: "Action",
        key: "Action",
        render: record => (
          <Icon
            type="delete"
            style={{ color: "red", fontSize: "26px" }}
            onClick={() => this.props.onRemoveFromCart(record.id)}
          />
        )
      }
    ];

    return (
      <Spin tip="Loading..." spinning={false}>
        <Layout>
          <Content style={{ padding: "0 50px" }}>
            <Breadcrumb style={{ margin: "40px 0" }} />
            <Layout style={{ padding: "24px 0", background: "#fff" }}>
              <h1 style={{ textAlign: "center", margin: "1rem" }}>
                Cart Items
              </h1>
              <Content style={{ padding: "0 24px", minHeight: 280 }}>
                <div className="content-container" />
                <Table
                  dataSource={dataSource}
                  columns={columns}
                  pagination={false}
                />
                <Button
                  style={{ float: "right", marginTop: "1rem" }}
                  type="primary"
                  icon="right"
                  disabled={cartItemsList.length === 0}
                  onClick={this.showConfirm}
                >
                  PROCEED TO CHECKOUT
                </Button>
              </Content>
            </Layout>
          </Content>
          <Footer style={{ textAlign: "center" }}>
          </Footer>
        </Layout>
      </Spin>
    );
  }
}

const mapStateToProps = state => {
  return {
    cartItemsList: selectors.makeSelectCartItemsList()(state)
  };
};

const mapDispatchToProps = dispatch => {
  return {
    // dispatching plain actions
    onRemoveFromCart: id => dispatch(removeFromCart(id))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Cart);
