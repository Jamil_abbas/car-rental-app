import axios from "axios";
import { takeLatest, all, put } from "redux-saga/effects";
import { replace } from "react-router-redux";
import { fetchListSuccess, fetchListError } from "./containers/App/actions";
import { FETCH_LIST } from "./containers/App/constants";
import { LOGIN_REQUEST } from "./containers/Login/constants";
import { loginSuccess } from "./containers/Login/action";

function* workerFetchList(action) {
  try {
    const response = yield axios.get("https://jamalaa.cc/jwtbackend/api/cars");
    yield put(fetchListSuccess(response.data));
  } catch (error) {
    yield put(fetchListError(error));
  }
}
function* workerLoginRequest(action) {
  yield put(loginSuccess(action.payload));

  if (action.payload.username !== "admin@admin.com") {
    console.log("1111111111");
    yield put(replace("/profile"));

    console.log("3333333");
  } else {
    console.log("22222222222222");

    yield put(replace("/admin"));
    console.log("44444444444");
  }
}

function* rootSaga() {
  yield all([
    takeLatest(FETCH_LIST, workerFetchList),
    takeLatest(LOGIN_REQUEST, workerLoginRequest)
  ]);
}

export default rootSaga;
