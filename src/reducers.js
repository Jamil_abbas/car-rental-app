import { cars_data } from "./cars_rental_api";
import { combineReducers } from 'redux-immutable';
import appReducer from "./containers/App/reducer";
const initialState = cars_data;

function cars (state = initialState, action) {
  switch (action.type) {
    default:
      return state;
  }
}

const carsReducer = combineReducers({
  cars,
  carsData: appReducer
});

export default carsReducer;
