import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router, Route } from "react-router-dom";
import App from "./containers/App";
import ContactUs from "./components/ContactUs";
import Navigation from "./containers/Navigation";
import SignUp from "./containers/SignUp";
import Login from "./containers/Login";
import Cart from "./containers/Cart";
import carsReducer from "./reducers";
import { createStore, applyMiddleware } from "redux";
import { Provider } from "react-redux";
import createSagaMiddleware from "redux-saga";
import rootSaga from "./sagas";
import { compose } from "redux"; // and your other imports from before
import Dashboard from "./containers/Dashboard";

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const sagaMiddleware = createSagaMiddleware();

const store = createStore(
  carsReducer,
  composeEnhancers(applyMiddleware(sagaMiddleware))
);
sagaMiddleware.run(rootSaga);

ReactDOM.render(
  <Provider store={store}>
    <Router>
      <Navigation />
      <Route exact path="/" component={App} />
      <Route path="/contact" component={ContactUs} />
      <Route path="/admin" component={Dashboard} />
      <Route path="/profile" component={Dashboard} />
      <Route path="/login" component={Login} />
      <Route path="/signUp" component={SignUp} />
      <Route path="/cart" component={Cart} />
      <Route path="/dashboard" component={Dashboard} />
    </Router>
  </Provider>,
  document.getElementById("root")
);
