import React from "react";
import faker from "faker";
import { Modal, Button, Row, Col, Icon } from "antd";
import "./style.css";

class CarItem extends React.Component {
  state = {
    visable: false,
    loading: false
  };

  showModal = () => {
    this.setState({
      visible: true
    });
  };

  handleOk = id => {
    this.props.onAddToCart(id);
  
    this.setState({
      visible: false
    });
  };

  handleCancel = () => {
    console.log("cancel");
    this.setState({ visible: false });
  };

  render() {
    const { id, year, brand, price, carimg } = this.props;
    const { loading, visible } = this.state;
    return (
      <>
        <div className="carItem" onClick={this.showModal}>
          <img
            src={carimg}
            alt="Smiley face"
            className="car-image-style"
            width={"100%"}
          />
          <div className="carItem-Content">
            <h4>brand: {brand}</h4>
            <h4>Year: {year}</h4>
            <h4>price: {price}</h4>
          </div>
        </div>
        <Modal
          visible={visible}
          width="55%"
          height={"55%"}
          onOk={() => this.handleOk(id)}
          onCancel={this.handleCancel}
          footer={[
            <Button key="back" onClick={this.handleCancel}>
              Back
            </Button>,
            <Button
              key="submit"
              type="primary"
              loading={loading}
              onClick={() => this.handleOk(id)}
            >
              Add to Cart
            </Button>
          ]}
        >
          <Row span={8}>
            <Col span={14} style={{ borderRight: "1.5px solid #03A9F4" }}>
              <img
                src={carimg}
                alt="Smiley face"
                className="-modal-car-image-style"
                width={"100%"}
              />
            </Col>
            <Col span={10}>
              <p className="modal-content supplier-details">Supplier Details</p>
              <div className="supplierRating">
                <p>
                  Name:{" "}
                  {(this.props.supplier &&
                    this.props.supplier.name &&
                    this.props.supplier.name) ||
                    faker.name}
                </p>
                <p>
                  Rating:{" "}
                  {(this.props.supplier &&
                    this.props.supplier.ratting &&
                    this.props.supplier.ratting) ||
                    faker.ratting}
                </p>
              </div>
              <p className="modal-content supplier-details">
                Car Specifications
              </p>
              <div className="supplierRating">
                {this.props.specs && this.props.specs.withAC && (
                  <p>
                    With Air Condition{" "}
                    <span>
                      {" "}
                      <Icon type="check-circle" theme="twoTone" />
                    </span>
                  </p>
                )}
                {this.props.specs && this.props.specs.mgbox && (
                  <p>
                    Manual GearBox{" "}
                    <span>
                      {" "}
                      <Icon type="check-circle" theme="twoTone" />
                    </span>
                  </p>
                )}
                {this.props.specs && this.props.specs.fourD && (
                  <p>
                    4+ Doors{" "}
                    <span>
                      {" "}
                      <Icon type="check-circle" theme="twoTone" />
                    </span>
                  </p>
                )}
              </div>
            </Col>
          </Row>
        </Modal>
      </>
    );
  }
}
export default CarItem;
