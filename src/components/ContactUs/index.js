import React from "react";
import {
  Layout,
  Spin,
  Breadcrumb,
  Icon,
  Button,
  Form,
  Input,
  Row,
  Col,
  Typography
} from "antd";
const { Content, Footer } = Layout;
const { TextArea } = Input;
const { Title } = Typography;

class ContactUsForm extends React.Component {
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log("----------------contact us from values----------------");
        console.log(values);
      }
    });
  };
  render() {
    const { getFieldDecorator } = this.props.form;

    return (
      <Spin tip="Loading..." spinning={false}>
        <Layout>
          <Content style={{ padding: "0 50px" }}>
            <Breadcrumb style={{ margin: "40px 0" }} />
            <Layout style={{ padding: "24px 0", background: "#fff" }}>
              <Title level={3} style={{ textAlign: "center", margin: "1rem" }}>
                Contact Us
              </Title>
              <Content style={{ padding: "0 24px", minHeight: 280 }}>
                <div className="content-container" />

                {/* <h1 style={{ textAlign: "center" }}>
                  PLEASE GIVE US YOUR DETAIL SO WE PUT IT HERE!
                </h1> */}
                <Row>
                  <Col span={8} offset={8}>
                    <Form onSubmit={this.handleSubmit} className="login-form">
                      <Form.Item>
                        {getFieldDecorator("email", {
                          rules: [
                            {
                              required: true,
                              type: "email",
                              message: "Please input your eamil!"
                            }
                          ]
                        })(
                          <Input
                            prefix={
                              <Icon
                                type="user"
                                style={{ color: "rgba(0,0,0,.25)" }}
                              />
                            }
                            placeholder="Email"
                          />
                        )}
                      </Form.Item>
                      <Form.Item>
                        {getFieldDecorator("message", {
                          rules: [
                            {
                              required: true,
                              message: "Please enter your message!"
                            }
                          ]
                        })(
                          <TextArea
                            rows={4}
                            cols={24}
                            size="large"
                            prefix={
                              <Icon
                                type="message"
                                style={{ color: "rgba(0,0,0,.25)" }}
                              />
                            }
                            placeholder="Your message"
                          />
                        )}
                      </Form.Item>
                      <Form.Item>
                        <Button
                          type="primary"
                          htmlType="submit"
                          className="contact-form-button"
                        >
                          Send
                        </Button>
                      </Form.Item>
                    </Form>
                  </Col>
                </Row>
              </Content>
            </Layout>
          </Content>
          <Footer style={{ textAlign: "center" }} />
        </Layout>
      </Spin>
    );
  }
}

const ContactUs = Form.create({ name: "contactus_form" })(ContactUsForm);
export default ContactUs;
